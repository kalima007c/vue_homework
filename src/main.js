import Vue from 'vue'
import App from './App.vue'
import axios from "axios"
Vue.config.productionTip = false
Vue.use(axios)
// App.config.globalProperties.axios=axios
new Vue({
  axios,
  render: h => h(App),
}).$mount('#app')
