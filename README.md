# vue_homework

## Clone repository
```
$git clone <url>
```
## Set up Step 1-2
## 1 : Set up package and dependencies
```
yarn or npm install 
```

## 2 : Set up package and dependencies
```
yarn serve or npm start
```

## (Optional) for build project to deploy
```
yarn build or npm run build
```